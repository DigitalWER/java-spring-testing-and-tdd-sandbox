package com.marcin.demo.tdd.service;

import com.marcin.demo.tdd.model.User;

public interface EmailVerificationService {
    void scheduleEmailConfirmation(User user);
}
