package com.marcin.demo.tdd.service;

import com.marcin.demo.tdd.exception.UserServiceException;
import com.marcin.demo.tdd.model.User;
import com.marcin.demo.tdd.repository.UserRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private EmailVerificationService emailVerificationService;

    @Override
    public User createUser(String id, String firstName, String lastName, String email, String password) {

        User user;

        if (id.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || password.isEmpty()) {
            throw new IllegalArgumentException("Some user data is missing");
        } else {
            user = new User(id, firstName, lastName, email, password);
        }

        boolean isUserCreated;

        try {
            isUserCreated = userRepository.save(user);
        } catch (RuntimeException ex) {
            throw new UserServiceException(ex.getMessage());
        }

        if (!isUserCreated) throw new UserServiceException("Could not create user");

        try {
            emailVerificationService.scheduleEmailConfirmation(user);
        } catch (RuntimeException ex) {
            throw new UserServiceException(ex.getMessage());
        }

        return user;
    }
}
