package com.marcin.demo.tdd.service;

import com.marcin.demo.tdd.model.User;

public interface UserService {
    User createUser(String firstName, String lastName, String email, String password, String s);
}
