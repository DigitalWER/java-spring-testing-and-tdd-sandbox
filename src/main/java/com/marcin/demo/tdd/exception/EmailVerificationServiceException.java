package com.marcin.demo.tdd.exception;

public class EmailVerificationServiceException extends RuntimeException{
    public EmailVerificationServiceException(String message) {
        super(message);
    }
}
