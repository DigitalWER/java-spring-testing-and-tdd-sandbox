package com.marcin.demo.tdd.repository;

import com.marcin.demo.tdd.model.User;

public interface UserRepository {
    boolean save(User user);
}
