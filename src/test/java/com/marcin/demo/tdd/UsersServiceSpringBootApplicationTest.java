package com.marcin.demo.tdd;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
public class UsersServiceSpringBootApplicationTest {

    @Test
    void contextLoads() {
        
    }
}
