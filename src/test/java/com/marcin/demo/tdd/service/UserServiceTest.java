package com.marcin.demo.tdd.service;

import com.marcin.demo.tdd.exception.EmailVerificationServiceException;
import com.marcin.demo.tdd.exception.UserServiceException;
import com.marcin.demo.tdd.model.User;
import com.marcin.demo.tdd.repository.UserRepository;
import com.marcin.demo.tdd.repository.UserRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepositoryImpl userRepository;
    @Mock
    private EmailVerificationServiceImpl emailVerificationService;
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    String expectedExceptionMessage = "Some user data is missing";


    @BeforeEach
    void init() {
        id = "1";
        firstName = "Marcin";
        lastName = "Marcin";
        email = "email";
        password = "123";
    }

    @Test
    void testCreateUser_whenUserIsCreated_returnedUserContainsAllData() {
        // Given
        when(userRepository.save(any(User.class))).thenReturn(true);

        // When
        User user = userService.createUser(id, firstName, lastName, email, password);

        // Then
        assertNotNull(user, "createUser() should not return null!");
        assertEquals(firstName, user.getFirstName(), "firstName is incorrect");
        assertEquals(lastName, user.getLastName(), "lastName is incorrect");
        assertEquals(email, user.getEmail(), "email is incorrect");
        assertEquals(password, user.getPassword(), "password is incorrect");
        assertNotNull(user.getId(), "id is missing");
        verify(userRepository).save(any(User.class));
    }

    @Test
    void testCreateUser_whenFirstNameIsEmpty_throwsIllegalArgumentException() {
        String firstName = "";
        // When
        var thrown = assertThrows(IllegalArgumentException.class, () -> userService.createUser(id, firstName, lastName, email, password), "Empty data should have caused the IllegalArgumentExcepting");

        assertEquals(expectedExceptionMessage, thrown.getMessage(), "Exception error message is not correct");
    }

    @Test
    void testCreateUser_whenLastNameIsEmpty_throwsIllegalArgumentException() {
        String lastName = "";
        // When
        var thrown = assertThrows(IllegalArgumentException.class, () -> userService.createUser(id, firstName, lastName, email, password), "Empty data should have caused the IllegalArgumentExcepting");

        assertEquals(expectedExceptionMessage, thrown.getMessage(), "Exception error message is not correct");
    }

    @Test
    void testCreateUser_whenSaveMethodThrowsException_thenThrowsUserServiceException() {
        // Given
        when(userRepository.save(any(User.class))).thenThrow(RuntimeException.class);
        // When
        var thrown = assertThrows(UserServiceException.class, () -> userService.createUser(id, firstName, lastName, email, password), "Should have thrown UserServiceException");
        // Then
    }

    @Test
    void testCreateUser_whenEmailNotificationExceptionThrown_throwsUserServiceException() {
        // Given
        when(userRepository.save(any(User.class))).thenReturn(true);

        doThrow(EmailVerificationServiceException.class).when(emailVerificationService).scheduleEmailConfirmation(any(User.class));

        // When
        assertThrows(UserServiceException.class, () -> userService.createUser(id, firstName, lastName, email, password), "Should have thrown UserServiceException");
        verify(emailVerificationService).scheduleEmailConfirmation(any(User.class));
    }

    @Test
    void testCreateUser_whenUserCreated_schedulesEmailConfirmation() {
        when(userRepository.save(any(User.class))).thenReturn(true);

        doCallRealMethod().when(emailVerificationService)
                .scheduleEmailConfirmation(any(User.class));

        //When
        userService.createUser(id, firstName, lastName, email, password);

        //Then
        verify(emailVerificationService).scheduleEmailConfirmation(any(User.class));
    }
}
